module.exports = function (grunt, options)
{
	return {
		options: {
			debounceDelay: 500,
			event: ['changed', 'added', 'deleted'],
			spawn: false
		},
		html: {
			files: ['<%= BuildConfigs.src.html %>/**/*.html', '!**/*_template.html'],
			tasks: ['sync:html_ToStaging']
		},
		html_ejs_templates: {
			files: ['<%= BuildConfigs.src.html_ejs %>/**/*.ejs'],
			tasks: ['ejs']
		},
		image_files: {
			files: [	'<%= BuildConfigs.src.images %>/**/*.gif',
						'<%= BuildConfigs.src.images %>/**/*.jpeg',
						'<%= BuildConfigs.src.images %>/**/*.jpg',
						'<%= BuildConfigs.src.images %>/**/*.png',
						'<%= BuildConfigs.src.images %>/**/*.svg'
			],
			tasks: ['copy:image_files_ToTemp', 'copy:image_files_ToStaging']
		},
		main_js: {
			files: ['<%= BuildConfigs.src.js %>/main.js'],
			tasks: ['sync:js_ToTemp', 'sync:js_ToStaging' ]
		},
		non_main_js: {
			files: ['<%= BuildConfigs.src.js %>/**/*.js', '!<%= BuildConfigs.src.js %>/main.js'],
			tasks: ['sync:js_ToTemp', 'sync:js_ToStaging' ]
		},
		css: {
			files: ['<%= BuildConfigs.src.css %>/**/*.css'],
			tasks: ['sync:css_ToTemp', 'sync:css_ToStaging']
		},
		sass: {
			files: ['<%= BuildConfigs.src.css %>/**/*.scss'],
			tasks: ['sass']
		},
		third_js: {
			files: ['<%= BuildConfigs.src.js_3P %>/**/*.js'],
			tasks: ['sync:js3P_ToTemp', 'sync:js_ToStaging' ]
		}
	}
};
