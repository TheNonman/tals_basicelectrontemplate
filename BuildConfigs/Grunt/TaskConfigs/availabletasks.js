module.exports = function(grunt, options)
{
	return {
		// NOTE: availabletasks expects a root 'tasks' object,
		// but 'load-grunt-tasks' handles root 'tasks' objects as an option to define and split out
		// configurations for multiple grunt tasks.
		//
		// To resolve this conflict, wrap the base availabletasks object inside a tasks object that
		// 'load-grunt-tasks' can handle. It will not handle the second 'tasks' object that is a
		// child of the contained 'availabletasks' object, letting that go into the actual task
		// config.
		tasks: {
			availabletasks: {
				tasks: {
					options: {
					  groups: {
						'Main User Tasks': [
						  'Docs',
						  'Nuke',
						  'Help',
						  'Work'
						]
					  }
					}
				}
			}
		}
	};
};
