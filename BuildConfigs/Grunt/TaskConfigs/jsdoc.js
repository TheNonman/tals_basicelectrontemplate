module.exports = function (grunt, options)
{
	return {
		dist: {
			src: [	'<%= BuildConfigs.src.js %>/**/*.js',
					'ReadMe.md',
					'!**/a_third_party/**'
			],
			options: {
				destination: 'Documentation/API_Docs',
				template : "node_modules/ink-docstrap/template",
				configure : "node_modules/ink-docstrap/template/jsdoc.conf.json"
			}
		}
	}
};
