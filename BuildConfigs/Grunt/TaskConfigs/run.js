module.exports = function (grunt, options)
{
	return {
		launch_electron: {
			cmd: 'npm',
			args: [
				'start'
			]
		},
		launch_hb: {
			cmd: 'node',
			args: [
				'BuildConfigs/HummingbirdScripts/launch_hb.js',
				'_Staging/index.html'
			]
		}
	}
};
