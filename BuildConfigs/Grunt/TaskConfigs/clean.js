module.exports = function (grunt, options)
{
	return {
		full: {
			options: {
				'no-write': false
			},
			src: [	'<%= BuildConfigs.temp.root %>',
					'<%= BuildConfigs.staging.root %>'
			]
		},
		full_test: {
			options: {
				'no-write': true
			},
			src: [	'<%= BuildConfigs.temp.root %>',
					'<%= BuildConfigs.staging.root %>'
			]
		},
		libs: {
			options: {
				'no-write': false
			},
			src: [ 	'_LibBuild',
					'__Temp/LibBuild'
			]
		},
		logs: {
			options: {
				'no-write': false
			},
			src: [ '__BuildLogs',
			]
		},
		strip_to_source: {
			options: {
				'no-write': false
			},
			src: [	'<%= BuildConfigs.temp.root %>',
					'<%= BuildConfigs.staging.root %>',
					'.sass-cache',
					'__Builds',
					'__BuildLogs',
					'node_modules'
			]
		}
	}
};
