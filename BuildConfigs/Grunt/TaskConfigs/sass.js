const sass = require('sass');

module.exports = function (grunt, options)
{
	return {
		options: {
			implementation: sass,
			sourceMap: true
		},
		dist: {
			files:
			{
				'<%= BuildConfigs.staging.css %>/styles.css': '<%= BuildConfigs.src.css %>/app_main.scss'
			}
		}
	}
};
