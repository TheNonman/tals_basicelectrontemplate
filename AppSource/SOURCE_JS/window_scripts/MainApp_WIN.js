// This file is loaded by the index.html file (via a non-blocking script tag) and it will
// be executed in the renderer process for that window.
//
// No Node.js APIs are available in this process because 'nodeIntegration' is turned off
// for BrowserWindows by defualt for inter-process security.
//
// Use a "preload script" associated to the BrowserWindow to define and register IPC
// (Inter-Process Communication) between the main process and window process scripts.
//
// This lets you selectively enable features needed in a given rendering process.

console.log("window_scripts -> MainApp_WIN.js :: Console Log (trace) from the Render Process.");
