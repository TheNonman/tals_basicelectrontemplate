
//
//  Imports / Requires
// ================================================================================
// ================================================================================

const { app, BrowserWindow, Menu, MenuItem } = require('electron');

const DevReload = require('electron-reload');
const ElectronDebug = require('electron-debug');
const Logger = require('electron-log');
const LogUserScope = Logger.scope('user');
const path = require('path');
const url = require('url');



//
//
//  Properties
// ================================================================================
// ================================================================================

let MainAppWindow_WIN;



//
//
//  App Init Thread
// ================================================================================
// ================================================================================

const RuntimeRootPath = path.join(__dirname, '../');
const WorkingElectronPath = path.join(__dirname, '../../', 'node_modules', '.bin', 'electron');

DevReload(RuntimeRootPath, { electron: WorkingElectronPath });

Logger.info("Basic Electron App Starting...");
LogUserScope.info("Here is a user scoped log...");

Logger.info("Runtime Context Root Path: " + RuntimeRootPath);
Logger.info("Runtime Context Electron Path: " + WorkingElectronPath);

ElectronDebug();

app.whenReady().then(
	() => {
		// Create the Main App Window
		// --------------------------------------
		CreateMainWindow();

		// Register App Event Handling
		// --------------------------------------
		app.on("activate", () => {
			// On macOS it's common to re-create a window in the app when the
			// dock icon is clicked and there are no other windows open.
			// Limited to macOS, because we quit the app when all windows are closed on other platforms
			if (BrowserWindow.getAllWindows().length === 0) {
				console.log("Reopen Main Window");
				CreateMainWindow();
			}
		});

		app.on('window-all-closed', () => {
			// On macOS it is common for applications and their menu bar
			// to stay active until the user quits explicitly with Cmd + Q
			if (process.platform !== 'darwin') {
				app.quit();
			}
		});
	}
);




//
//
//  Window Creation and Setup Methods
// ================================================================================
// ================================================================================

function CreateMainWindow() {
	MainAppWindow_WIN = new BrowserWindow({
		width: 800,
		height: 600,
		webPreferences: {
			preload: path.join(__dirname, './window_preload_scripts/MainAppWin_IPC.js')
		}
	});

	// General loadURL method
	// ================================================
	// If used to load a local file, set the path via NodeJS url.format and add the 'file:' protocol and slashes: true
	// Resolving a relative path from the currently executing JavaScript file (via '__dirname') helps by using a relative path that
	// will work in both development and packaged environments/modes.
	/*
	MainAppWindow_WIN.loadURL(url.format({
		pathname: path.join(__dirname, "../index.html"),
		protocol: "file:",
		slashes: true
	}));
	*/

	// Simple loadFile method
	// Note that the filePath is a path to an HTML file relative to the root of the application
	// MainAppWindow_WIN.loadFile('_Staging/index.html');//

	// As with the general loadURL method, resolving a relative path from the currently executing JavaScript file (via '__dirname')
	// helps by using a relative path that will work in both development and packaged environments/modes.
	MainAppWindow_WIN.loadFile(path.join(__dirname, "../index.html"));

	MainAppWindow_WIN.on('closed', () => {
		// Dereference the window object, usually you would store windows
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		MainAppWindow_WIN = null;
	});
};

// TODO : Wrap in Env Check
//require('electron-debug')();
