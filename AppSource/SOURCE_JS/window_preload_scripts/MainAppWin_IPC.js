// "Preload scripts" contain code that executes in a renderer process before its web content
// begins loading. These scripts run within the renderer context, but are granted more privileges
// by having access to Node.js APIs.

// All of the Node.js APIs ( including require() ) are available in the preload process. It has
// the same sandbox as a Chrome extension.

console.log("window_preload_scripts -> MainAppWin_IPC.js :: Console Log (trace) from the Render Process' Preload.");

window.addEventListener('DOMContentLoaded', () => {
	const replaceText = (selector, text) => {
		const element = document.getElementById(selector)
		if (element) element.innerText = text
	}

	for (const type of ['chrome', 'node', 'electron']) {
		replaceText(`${type}-version`, process.versions[type])
	}
})
