# Tal's Basic Electron Template

# Prerequisites / Dependencies

Use of this template assumes the following installed on the target computer/environment:

1. [NodeJS](https://nodejs.org) --  (latest tested version 16.13.1)

1. NodeJS NPM (installed with NodeJS)

1. The Grunt-CLI installed Globally  (`npm install -g grunt-cli`)

# Scaffolding a New Electron App Project

*Assuming prerequisites are already installed on the dev machine...*

1. Create an empty directory for a new project (No NPM package.json, see 'First Steps' below).

1. Open a Command Line / Terminal targeting the empty local project directory.

1. Execute: `npm install git+https://TheNonman@bitbucket.org/TheNonman/tals_basicelectrontemplate --no-save`

1. Wait for the scaffolding process to complete, then...

1. Execute: `grunt Work`

----

## Immediate ToDos

- Support for Env variables and activate Electron-Debug & Devtron based on Env variables.
- Definintion in Electron-Builder for various package types to build.

## Next Step (After above)

- Start separate project for "Tal's Electron React Template"

----

## About this Basic Electron Template

This is a template project repo that can be used to quickly scaffold a new super-simple Electron app.

The resulting, scaffolded project contains just bare essentials to kick start a cross-platform Electron app project. Thus, it includes no JavaScript or CSS frameworks, etc. That said, SASS support is included (with Grunt task integration) to enable compiling SASS and/or SCSS to CSS.

Included in the Scaffolded Project:

- [Electron](https://www.electronjs.org/)  (^18.1.0)
- [Electron-Builder](https://www.electron.build/)  (^23.0.3)
   - For generating "packed" builds of your Electron app and also for generating distributables of your "packed" Electron app builds (ZIPs, DMGs, Installers, etc.) for various platforms. 
- [Electron-Debug](https://www.npmjs.com/package/electron-debug)  (^3.2.0)
   - For useful debug features in your Electron app work flow.
- [Electron-Log](https://www.npmjs.com/package/electron-log)  (^4.4.6)
   - Simple setup for logging both to file and console.
- [Electron-Store](https://www.npmjs.com/package/electron-store)  (^8.0.0)
   - Simple setup storage of persist user preferences and other app data.
- [Electron-ProgressBar](electron-progressbar)  (^2.0.1)
   - Simple setup for displaying progress dialogs (either modal or non-modal).
- [Electron-Rebuild](https://www.npmjs.com/package/electron-rebuild)  (^3.2.7)
   - For Validating that your project's Native Modules are built for the target Electron version (see below).
- [Electron-Reload](https://www.npmjs.com/package/electron-reload)  (^1.5.0)
   - For reloading active BrowserWindows in Electron when source files change.
- [SASS](https://sass-lang.com/)  (dart-sass implementation, ^1.50.1)
   -  To enable compiling SASS and/or SCSS to CSS for your project.
- [Winston](https://github.com/winstonjs/winston) (^3.1.0)
   - Strong logging support both in and outside of Electron.
- [Devtron](https://www.npmjs.com/package/devtron)  (REMOVED as Devtron is currently broken and [unmaintained](https://github.com/electron-userland/devtron/issues/200) )
   - Provided a number of great debugging features until it stopped working :(


## First Steps

Edit the generated NPM package.json with info about your project. As an aside (and as noted above), **do *not* run** "npm init" or create an NPM package.json file in your project directory before installing the scaffold. The scaffolding installation process will add a package.json for you, ready for editing, later reinstalling of your project, updating, etc.

Edit the generated ReadMe.md with info about your project.

Open the generated Documentation folder, edit the starting 01\_About.md with info about your project and fill in more documentation details as you go. If you include JSDoc comments in your JavaScript source files, you can execute `grunt Docs` to have JSDoc HTML documentation files added in "Documentation -> API\_Docs" this generated documentation will be ignored by Git and thus not stored in your version control repo.

Don't forget to save your scaffolded project off to version control.

Edit, create, enjoy.

## Cleanup

Projects scaffolded with 'Tal's Basic Electron Template' can be spun up for testing quickly and can be torn down to cleanup disk space just as quickly. After playing around, testing and working for a while, simply run `grunt Nuke` to clear out all resources that are not part of your project's source code, including anything installed in node_modules. The next time you want to run and work with the project, you just run `npm install` and then `grunt Work` again.

## Grunt Task Help

Execute `grunt Help` or just `grunt` to get a list of available Grunt tasks, those tasks that are particularly intended to be called directly by users are specifically called out in the display.

## Additional Documentation

As part of the scaffolding, a 'Documentation' directory is added at the project root for... well... documentation. 

This directory will initially contain a copy of this ReadMe and any additional documentation about the template itself. Of course, the intent of this folder is that you can use it to also store documentation regarding *your* project. This includes any documentation generated by including JSDoc comments in your JavaScript source files.

As noted above, simply execute `grunt Docs` to have JSDoc generate HTML documentation files based on your JS file comments. The generated documentation will be written to an  "API\_Docs" folder inside the base "Documentation" folder. Note that any 'Documentation\/API\_Docs' folder (and its content) will be ignored by Git.

### Working Electron Builds

- Standard (with Grunt build tasking support)
    - `grunt Work`
- Electron execution only (requires a previous Grunt build to have been completed)
    - `npm start`

### Electron App Packaging

`npm run pack`

### Package Distributable App Installers

`npm run dist`


# Rebuilding Native Modules for the Targeted Electron vs NodeJS Versions

From the ElectronJS documentation on ['Native Node Modules'](https://www.electronjs.org/docs/latest/tutorial/using-native-node-modules):

> "Native Node.js modules are supported by Electron, but since Electron has a different application binary interface (ABI) from a given Node.js binary (due to differences such as using Chromium's BoringSSL instead of OpenSSL), the native modules you use will need to be recompiled for Electron. Otherwise, you will get the following class of error when you try to run your app:"
> 
> `Error: The module '/path/to/native/module.node' `
> `was compiled against a different Node.js version using `
> `NODE_MODULE_VERSION $XYZ. This version of Node.js requires `
> `NODE_MODULE_VERSION $ABC. Please try re-compiling or re-installing `
> `the module (for instance, using 'npm rebuild' or 'npm install').`

Basically, this means that Native Modules used in an Electron app (and by the Electron embedded version of NodeJS) may need to vary from those that are needed/used by the version of the NodeJS binary that your environment is using. Any Native Modules used in an Electron project, need to be built for the version of Electron (and embedded NodeJS) that the project is targeted to use.

In general, this will only become an issue if you attempt to work on a project that will use Native Modules. When this is the case, you can of course try just installing the Native Modules that you need, bind them into your Electron app and see if you're all good. 

If you run into errors like the one shown above (i.e. *...'was compiled against a different Node.js version'...*), this templating project includes **Electron-Rebuild**, which can be used to make sure that your project's Native Modules are built to support the version of Electron you're building for and using in your work flow. Just execute the following in your project:

`./node_modules/.bin/electron-rebuild`

This should get your Electron builds working with your Native Modules. The extra thing to note here is that you'll likely need to re-run the Electron-Rebuild process any time you... 

- Add another Native Module
- Do a clean (NPM) install (i.e. `npm ci`) 
- Install on a new environment

Finally, if you put together an Electron project using Native Modules that you are looking to have combined with some other distributable target (i.e. non-electron), you may well find that your Electron builds need one Native Modules build (via Electron-Rebuild) while building for your other target requires a different Native Modules build.

> *This author ran into this issue when attempting to work on a NodeJS CLI app and an Electron GUI for that CLI's main library process in the same project. The Electron builds needed one set of built Native Modules, while executing the CLI from the same project space needed a different set of built Native Modules (in the project space's node_modules) for the version of NodeJS that I was running in my environment.*

Should you run into this issue (or a similar case), it may be useful as an aside, to consider the following for reference. Swapping rebuilds can to accomplish by...

- When trying to run a non-Electron process that needs the Native Modules (e.g. the CLI example):
   - Do a clean install of the project's modules (which will get your Native Modules built for the active environment version of NodeJS)...
   - `npm ci`
- When needing to build for an Electron app:
   - Force a rebuild of the Native Modules for the targeted version of Electron by using [Electron-Rebuild](https://github.com/electron/electron-rebuild)...
   - `./node_modules/.bin/electron-rebuild`

Naturally, it's suggested that you avoid creating this kind of rebuilding pain... In the case of the example project discussed above, this author split the working project for the CLI from the Electron GUI project and just made the CLI a dependency of the Electron app.
