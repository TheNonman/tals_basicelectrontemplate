'use strict';
const fs = require('fs');
const path = require('path');
const Chalk = require("chalk");

//
//
// # NOTES
// ================================================================================================
// ================================================================================================



//
// # GRUNT / NODE FILE MATCHING (GLOBBING)
// ----------------------------------------------
//
// Restrict matching to the content of one directory level down (i.e. single directory nesting):
//		'SomeRootDirectory/{,*/}*.js'
// Full Recursion, matching the content files in all subdirectoy levels:
//		'SomeRootDirectory/**/*.js'
// Full Recursion, matching All content:
//		'SomeRootDirectory/**'
// Skip a directory during recursion:
//		'!**/SkipThisDirectoryName/**'


//
//
// # Initialization of Any External Processor Objs
// ================================================================================================
// ================================================================================================

var LogFile = require("logfile-grunt");
var ElectronWorkProcess = null;


//
//
// # Get Available Environment Variables
// ================================================================================================
// ================================================================================================
const projectRoot = path.resolve(__dirname);

let EnvVars = null;
if (fs.existsSync(path.resolve(projectRoot, 'build_settings.env'))) {
	EnvVars = require('dotenv').config({ path: path.join(projectRoot, 'build_settings.env' ) });
}
else {
	if (fs.existsSync(path.resolve(projectRoot, 'build_settings_default.env'))) {
		EnvVars = require('dotenv').config({ path: path.join(projectRoot, 'build_settings_default.env' ) });

		console.log(Chalk.yellow("WARNING :  Using Default Build Settings file ('build_settings_default.env'). To customize settings, duplicate the Defaults File ('build_settings_default.env') and rename it: build_settings.env"));
	}
	else {
		console.log(Chalk.red("WARNING :  No Valid Build Settings file ('.env') Available !!!."));
	}
}

// Validate Environment Vars
process.env.NODE_ENV = process.env.NODE_ENV || 'development';



//
//
// # Definition of Grunt Task Handling
// ================================================================================================
// ================================================================================================

module.exports = function(grunt)
{
	//
	// # Load Sub-modules (i.e. Grunt Tasks)
	// (Simplify with "matchdep" or "load-grunt-tasks" or get fancy with "load-grunt-config")
	// --------------------------------------------------------------------------------------------

	const exec = require('child_process').exec;
	const os = require('os');
	const runtimePlatform = os.platform().startsWith('win') ? 'win' : 'nix';

	// Load All Grunt Task Types (i.e. All Node Modules that Start with "grunt-")
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
	require('matchdep').filter('grunt-*').forEach(grunt.loadNpmTasks);

	// Load All Available Custom Grunt Tasks
	//grunt.task.loadTasks('./BuildConfigs/Grunt/CustomTasks');


	//
	// # Configure App Structure References
	// --------------------------------------------------------------------------------------------
	var NodePkg = grunt.file.readJSON('package.json');

	var BuildConfigs = {
		src: {
			root: "AppSource",
			css: "AppSource/SOURCE_STYLES",
			css_3P: "AppSource/SOURCE_STYLES/a_third_party",
			fonts: "AppSource/SOURCE_FONTS",
			fonts_to_build: "AppSource/SOURCE_SVG4FONTS",
			html: "AppSource/SOURCE_HTML",
			html_ejs: "AppSource/SOURCE_HTML-EJS",
			images: "AppSource/SOURCE_IMAGES",
			js: "AppSource/SOURCE_JS",
			js_3P: "AppSource/SOURCE_JS/a_third_party",
			resources: "AppSource/SOURCE_BUILD_RESOURCES",
			templates: "AppSource/SOURCE_HTML/templates"
		},
		temp: {
			root: "__Temp",
			css: "__Temp/css",
			css_3P: "__Temp/css/a_third_party",
			css_pre_source: "__Temp/css_compass_source",
			fonts: "__Temp/fonts",
			fonts_built: "__Temp/fonts_built",
			images: "__Temp/images",
			js: "__Temp/js",
			js_3P: "__Temp/js/a_third_party",
			templates: "__Temp/templates"
		},
		staging: {
			root: "_Staging",
			css: "_Staging/css",
			css_3P: "_Staging/css/a_third_party",
			fonts: "_Staging/fonts",
			images: "_Staging/images",
			js: "_Staging/js",
			js_3P: "_Staging/js/a_third_party",
			templates: "_Staging/templates"
		},
		logs: {
			base: "__BuildLogs"
		},
		finalFileNames: {
			css_Print: 'print.css',
			css_Screen: 'screen.css',
			js_Global: 'global.js'
		}
	};

	var JSHintOptions = {
		Production: {
			debug: false,
			node: true,
			browser: true,
			jquery: true, // Support for jQuery $ Def
			esnext: true,
			bitwise: true,
			curly: false,
			eqeqeq: false, // Suppress Warnings About Using == instead of ===
			immed: true,
			latedef: true,
			newcap: false,
			noarg: true,
			regexp: true,
			undef: true,
			unused: 'vars',  // Warn About Unused Variables but not Function Params (like e in an Event Handler)
			strict: true,
			trailing: false, // Allow Trailing Whitespace
			smarttabs: true,
			laxbreak: true,
			laxcomma: true, // Allow Comma-first Object Definition Syntax
			white: false // Don't Warn About Whitepace/tab Counts
		},
		Debug : {
			debug: true,
			node: true,
			browser: true,
			jquery: true, // Support for jQuery $ Def
			esnext: true,
			bitwise: true,
			curly: false,
			eqeqeq: false,  // Suppress Warnings About Using == instead of ===
			immed: true,
			latedef: true,
			newcap: false,
			noarg: true,
			regexp: true,
			undef: true,
			unused: 'vars',  // Warn About Unused Variables but not Function Params (like e in an Event Handler)
			strict: true,
			trailing: false, // Allow Trailing Whitespace
			smarttabs: true,
			laxbreak: true,
			laxcomma: true, // Allow Comma-first Object Definition Syntax
			white: false // Don't Warn About Whitepace/tab Counts
		}
	};


	var LogFileOptions = {
		filePath: BuildConfigs.logs.base + "/Grunt.log",
		clearLogFile: true,
		keepColors: false,
		textEncoding: "utf-8"
	};

	var GruntBuildOptions = {
								config: { src: "./BuildConfigs/Grunt/TaskConfigs/*.js" },
								NodeProjectPkg: NodePkg,
								BuildConfigs: BuildConfigs,
								JSHintOptions: JSHintOptions,

								ElectronWorkProcess: ElectronWorkProcess
							};

	var TaskConfigs = require('load-grunt-configs')(grunt, GruntBuildOptions);


	//
	// # Helper Method Definitions
	// --------------------------------------------------------------------------------------------



	//
	// # Configure Grunt Tasks
	// --------------------------------------------------------------------------------------------

	grunt.initConfig(TaskConfigs);


	//
	// # Register (Executable) Grunt Task Aliases
	// --------------------------------------------------------------------------------------------

	// Define the DEFAULT task - executed when running grunt with no arguments
	grunt.registerTask('default', ['Help']);


	grunt.registerTask('BasicBuild', 'Basic One-time build (No Watches Set).',
		function() {
			grunt.task.run([
				'clean:logs',
				'StartProcessLog',
				'clean:full',

				'sync:js_ToTemp',
				'sync:js3P_ToTemp',
				'sync:css_ToTemp',
				'sync:css_ToStaging',
				'sync:js_ToStaging',
				'sync:html_ToStaging',
				'sass',
				'copy:image_files_ToTemp',
				'copy:image_files_ToStaging',
				'sync:fonts_ToStaging',
				'copy:app_build_sttings'
			]);
		}
	);

	grunt.registerTask('JustClean', 'Basic One-time build (No Watches Set).',
		function() {
			grunt.task.run([
				'clean:logs'
			]);
		}
	);

	grunt.registerTask('ProdBuild', 'One-time Production build (No Watches Set).',
		function() {
			grunt.task.run([
				'clean:logs',
				'StartProcessLog',
				'clean:full',

				'sync:html_ToStaging',
				'copy:image_files_ToTemp',
				'copy:image_files_ToStaging',
				'sync:fonts_ToStaging'
			]);
		}
	);


	grunt.registerTask('Docs', 'Generate (Update) the API documentation with JSDoc.',
		function() {
			grunt.task.run([
				'StartProcessLog',
				'jsdoc'
			]);
		}
	);


	grunt.registerTask('Help', 'Print project info and available grunt tasks.  < DEFAULT >',
		function() {
			grunt.task.run([
				'StartProcessLog',
				'availabletasks'
			]);
		}
	);


	grunt.registerTask('Nuke', 'Clears All Build Content and Strips Down to Only Source (NPM Install will be Required to Work Again).',
		function() {
			grunt.task.run([
				'clean:strip_to_source'
			]);
		}
	);


	grunt.registerTask('Work', 'Starts the Development Build Cycle with Watches.',
		function() {
			grunt.task.run([
				'BasicBuild',
				'SpawnWorkingElectron',
				'watch'
			]);
		}
	);


	// Default task(s).
	grunt.registerTask('StartProcessLog', 'Adds Process Info in the Log and Console',
		function() {

			new LogFile(grunt, LogFileOptions);

			grunt.log.writeln();
			grunt.log.writeln();
			grunt.log.writeln();
			grunt.log.writeln(Chalk.green('Starting Grunt Tasking for...'));
			grunt.log.writeln(Chalk.green('	->	NPM Project Name: ' + grunt.config('NodeProjectPkg.name')));
			grunt.log.writeln(Chalk.green('	->	NPM Project Version: ' + grunt.config('NodeProjectPkg.version')));
			grunt.log.writeln(Chalk.green('	->	Using Grunt Version: ' + Chalk.blue(grunt.version)));
			grunt.log.writeln(Chalk.green('	->	Node Environment: ' + Chalk.blue(process.env.NODE_ENV)));

			var startTime = new Date();
			grunt.log.writeln(Chalk.green('	->	Process Started: ' + Chalk.blue(startTime)));
			grunt.log.writeln(Chalk.green('================================================================================'));
			grunt.log.writeln(Chalk.green('================================================================================'));
			grunt.log.writeln("");
		}
	);


	grunt.registerTask('SpawnWorkingElectron', function() {
		if (ElectronWorkProcess) {
			grunt.log.writeln(Chalk.yellow('WARNING: Restarting the Electron Work Process via Grunt Tasking is Not Supported !'));
			grunt.log.writeln(Chalk.yellow('To restart the main Electron work process when the main Electron process is changed, use Electron-Reload in your Electron app.'));
			grunt.log.writeln(Chalk.yellow('SpawnWorkingElectron should only be called in a stratup task.'));
		}

		// (re-)start ElectronWorkProcess
		let electronExecutionPath = path.resolve('./node_modules/.bin/electron');
		if (runtimePlatform !== 'win')		electronExecutionPath = './node_modules/.bin/electron';
		ElectronWorkProcess = exec(electronExecutionPath + ' .',
			function (err, stdout, stderr) {
				grunt.log.write(stdout);
				grunt.log.error(stderr);
				if (err !== null) {
					grunt.log.error('exec error: ' + err);
				}
		});

		// when grunt exits, make sure ElectronWorkProcess quits too
		process.on('exit', function() {
			grunt.log.writeln('\nExit Grunt Process');
			if (ElectronWorkProcess) {
				grunt.log.writeln('Killing Electron Work Process');
				ElectronWorkProcess && ElectronWorkProcess.kill();
			}
			else {
				grunt.log.writeln(Chalk.yellow('Grunt lost access to the Electron Work Process, you\'ll need to quit it yourself ;)'));
				grunt.log.writeln('This happens if you Quit the Electron app yourself or the Work Process is restarted separate from Grunt (e.g. by Electron-Reload).');
			}
		});

		ElectronWorkProcess.stdout.on('data', (data) => {
			grunt.log.writeln(data.toString());
		});

		ElectronWorkProcess.stderr.on('data', (data) => {
			grunt.log.writeln(data.toString());
		});

		ElectronWorkProcess.on('exit', (code) => {
			grunt.log.writeln(`ElectronWorkProcess exited with code ${code}`);
			ElectronWorkProcess = null;
		});

	});

};
