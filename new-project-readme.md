
# Introduction

**TODO**: Give a short introduction of your project. Let this section explain the objectives, motivation, summary of this project. 

# Prerequisites / Dependencies

This project is based on [Tal's Basic Electron Template](https://bitbucket.org/TheNonman/tals_basicelectrontemplate) and all of the same / usual requirements apply (for info on the features and support provided by the template, refer to its ReadMe in the Documentation folder [here](./Documentation/zzz_Tals_BasicElectronTemplate_ReadMe.md)).


1. [NodeJS](https://nodejs.org) --  (latest tested version 16.13.1)

1. NodeJS NPM (installed with NodeJS)

1. The Grunt-CLI installed Globally  (`npm install -g grunt-cli`)

# Getting Started

Assuming that you have the prerequisites installed on your environment...

1. Sync this Git repo

2. Open a Command Line / Terminal targeting the local repo directory.

   1. Execute `npm install`

   1. Execute: `grunt Work`

Grunt will build a staging directory from the content in AppSource and launch the built content in an Electron working environment with dev file watches set for changes.

When ready, use Control-C to shut down the Electron instance and watches.

# Grunt Task Help

Execute `grunt Help` or just `grunt` to get a list of available tasks, those tasks that are particularly intended to be called directly by users are specifically called out in the display.

# Cleanup

Projects scaffolded with 'Tal's Basic Electron Template' can be spun up for testing quickly and can be torn down to cleanup disk space just as quickly. After playing around, testing and working for a while, simply run `grunt Nuke` to clear out all resources that are not part of your project's source code, including anything installed in node_modules. The next time you want to run and work with the project, you just run `npm install` and then `grunt Work` again.


# Additional Documentation

Additional info and documentation can be found in the project's 'Documentation' directory.


# Markdown and ReadMe References

If you want to learn more about editing this ReadMe and creating good markdown-based readme files, refer the following sources:

* [Adam Pritchard's Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

* [Markdown-Guide](http://markdown-guide.readthedocs.io/en/latest/basics.html)

* [Visual Studio ReadMe Guidelines](https://www.visualstudio.com/en-us/docs/git/create-a-readme)


